![](images/DevOps.JPG)
# DevOps Managed Service

#### THINK DevOps ! THINK Automation !

## Project Details

#### Repositories based on DevOps Applications:
- [Artifactory](Artifactory)
- [GitHub](GitHub)
- [GitLab](GitLab)
- [Perforce](Perforce)
- [Monitoring](Monitoring) 
 
#### Useful scripts:
- [Fetch_ADusers](Fetch_ADusers)    